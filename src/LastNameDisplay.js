import React from 'react'

class LastNameDisplay extends React.PureComponent{
    render() {
        console.log('LastNameDisplay changed');

        return (
            <span>{this.props.lastName}</span>
        )
    }
}

export default LastNameDisplay

// function LastNameDisplay(props) {
    
//     return (
//         <span>{props.lastName}</span>
//     )
// }