import React from 'react';
import './App.css';
import UsernameForm from './UsernameForm.js';
import LastNameForm from './LastNameForm.js'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h2>Homework 1</h2>

        <UsernameForm/>
        <LastNameForm lastName='Doe' />
      </header>
    </div>
  );
}

export default App;
