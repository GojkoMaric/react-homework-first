import React, { Component } from 'react';
import UsernameDisplay from './UsernameDisplay'

class UsernameForm extends Component {
    constructor() {
        super()
        this.state = {
            username: "John",
            newUsername: ''
        }
    }

    handleChange = (e) => {
        this.setState({newUsername: e.target.value})
    }

    usernameSubmit = () => {
        this.setState({
            username: this.state.newUsername,
            newUsername: ''
        })
    }
    
    render() {
        return(
            <div>
                <h3>Username: <UsernameDisplay username={this.state.username}/></h3>
                <input name='username' placeholder='username' onChange={this.handleChange} value={this.state.newUsername} />
                <button onClick={this.usernameSubmit}>Change Username</button>
            </div>
        )
    };
}

export default UsernameForm;