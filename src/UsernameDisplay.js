import React from 'react'

function UsernameDisplay(props) {
    // console.log('UsernameDisplay changed');
    
    return (
        <span>{props.username}</span>
    )
}

export default React.memo(UsernameDisplay);