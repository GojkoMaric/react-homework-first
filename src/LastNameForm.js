import React, { Component } from 'react';
import LastNameDisplay from './LastNameDisplay'

class LastNameForm extends Component {
    constructor() {
        super();
        this.state = {
            lastName: "Doe",
            newLastName: ''
        }
    }
    
    lastNameChange = e => this.setState({newLastName: e.target.value})

    lastNameSubmit = e => this.setState({lastName: this.state.newLastName, newLastName: ''})

    render() {
        return (
            <div>
                <h3>Last name: <LastNameDisplay lastName={this.state.lastName}/></h3>
                <input name='lastName' placeholder="Last Name" onChange={this.lastNameChange} value={this.state.newLastName} />
                <button onClick={this.lastNameSubmit}>Change Last Name</button>
            </div>
        )
    }
}

export default LastNameForm;